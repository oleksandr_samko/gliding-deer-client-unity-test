using System.Collections.Generic;
using System.Runtime.CompilerServices;
using UnityEngine;
using UnityEngine.UI;
using static UnityEngine.Debug;

public class MicrobesView : MonoBehaviour
{
    [SerializeField] private List<Microbe> _microbes;
    [SerializeField] private Text _displayedText;
    
    private float _totalHealth;
    private float _avgHealth;
    
    private float _interval = 3;

    void FixedUpdate()
    {
        if (Time.frameCount % _interval == 0)
        {
            CountHealth();
        }
        else if (Time.frameCount % _interval == 1)
        {
            DisplayHealth();
        }
    }

    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    private void CountHealth()
    {
        _totalHealth = 0;
        foreach (Microbe microbe in _microbes)
        {
            _totalHealth += microbe.Health;
        }

        _avgHealth = _totalHealth / _microbes.Count;
    }

    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    private void DisplayHealth()
    {
        _displayedText.text = string.Concat("Total microbes: ",_microbes.Count.ToString()," Avg health ",_avgHealth.ToString());
        Log(_displayedText.text);
    }
}