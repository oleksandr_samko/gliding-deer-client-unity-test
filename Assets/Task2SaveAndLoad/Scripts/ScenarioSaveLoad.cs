using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Task2SaveAndLoad;

public class ScenarioSaveLoad : MonoBehaviour
{
    [SerializeField] private SampleClass _testObj1;
    [SerializeField] private SampleClass _testObj2;
        
    void Start()
    {
        SaveManagerPrefs.Delete(_testObj1.ToString());
        SaveManagerPrefs.Save(_testObj1,_testObj1.ToString());
        try
        {
            SaveManagerPrefs.Load(out _testObj2, _testObj2.ToString());
        }
        catch (Exception e)
        {
            Debug.LogWarning(e.Message);
        }
    }
    
}
