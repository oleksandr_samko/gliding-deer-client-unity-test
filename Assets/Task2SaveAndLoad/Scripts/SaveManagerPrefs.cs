using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using System.Xml;
using System.Xml.Serialization;

namespace Task2SaveAndLoad
{
    public static class SaveManagerPrefs
    {
        public static void Save<T>(T saveObj, string saveName)
        {
            XmlSerializer serializer = new XmlSerializer(typeof(T));
            using (StringWriter sw = new StringWriter())
            {
                serializer.Serialize(sw, saveObj);
                PlayerPrefs.SetString(saveName, sw.ToString());
            }
        }

        public static void Load<T>(out T loadObj, string loadName)
        {
            string savedObjText = PlayerPrefs.GetString(loadName);
            if (String.IsNullOrEmpty(savedObjText))
            {
                throw new NullReferenceException("No saving data " + loadName);
            }

            XmlSerializer serializer = new XmlSerializer(typeof(T));
            using (var reader = new StringReader(savedObjText))
            {
                loadObj = (T) serializer.Deserialize(reader);
            }
        }

        public static void Delete(string saveName)
        {
            PlayerPrefs.DeleteKey(saveName);
        }
    }
}