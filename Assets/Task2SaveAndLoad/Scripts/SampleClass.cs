using System;

namespace Task2SaveAndLoad
{
    [Serializable]
    public class SampleClass
    {
        public int valueInt = 0;
        public float valueFloat = 0f;
        public string valueString = "";
    }
}