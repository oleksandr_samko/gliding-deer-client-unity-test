using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SubmarineGame
{
    public class Board
    {
        public Cell[,] Cells;
        private int _maxBigSubmarine = 4;

        public void PrintAllWithoutExploded()
        {
            int startIndex;
            int endIndex;
            int lastID = -1;
            for (int i = 0; i < Cells.GetLength(0); i++)
            {
                for (int j = 0; j < Cells.GetLength(1); j++)
                {
                    //find submarine
                    startIndex = j;
                    endIndex = startIndex;
                    if (Cells[i, j].Occupied)
                    {
                        if (lastID != Cells[i, j].SubmarineID)
                        {
                            
                        }
                    }
                    
                    
                    //check explotion
                    
                    
                    //print id or not
                }
            }
            
        }

        public void PrintAllWithoutExploded2()
        {
            int startIndex = -1;
            int endIndex = -1;
            int lastID = -1;
            int submarineIndex = 0;
            bool isDestroyed = false;
            bool[] isExplosion = new bool[_maxBigSubmarine];
            for (int i = 0; i < Cells.GetLength(0); i++)
            {
                for (int j = 0; j < Cells.GetLength(1); j++)
                {
                    if (!Cells[i, j].Occupied)
                    {
                        if (Cells[i, j].MineExist)
                        {
                            continue;
                        }

                        continue;
                    }

                    //new submarine
                    if (lastID != Cells[i, j].SubmarineID)
                    {
                        isDestroyed = CheckDestroyedSubmarine(isExplosion, submarineIndex);
                        if (isDestroyed)
                        {
                            //for (int i = 0; i < UPPER; i++)
                            //{
                            //}
                        }

                        lastID = Cells[i, j].SubmarineID;
                        startIndex = j;
                        endIndex = startIndex;
                        submarineIndex = -1;
                    }

                    if (Cells[i, j].MineExist)
                    {
                        submarineIndex++;
                        isExplosion[submarineIndex] = true;
                    }
                    else
                    {
                        submarineIndex++;
                        isExplosion[submarineIndex] = false;
                    }
                }
            }
        }

        private bool CheckDestroyedSubmarine(bool[] isExplosion, int size)
        {
            for (int i = 0; i < size; i++)
            {
                if (!isExplosion[i])
                {
                    return false;
                }
            }

            return true;
        }
    }
    
    public struct Cell
    {
        public bool Occupied;
        public bool MineExist;
        public int SubmarineID;
    }
}