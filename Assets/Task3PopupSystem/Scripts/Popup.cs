using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

namespace Task3PopupSystem
{
    public class Popup : MonoBehaviour
    {
        [SerializeField] private Text _title;
        [SerializeField] private Text _body;
        [SerializeField] private Button[] _buttons;
        private LayoutElement[] _buttonLayout;
        private Text[] _buttonsText;

        public float DefautWidth { get; } = 100;
        
        private void Awake()
        {
            gameObject.SetActive(false);
            _buttonsText = new Text[_buttons.Length];
            _buttonLayout = new LayoutElement[_buttons.Length];
            for (int i = 0; i < _buttons.Length; i++)
            {
                _buttonsText[i] = _buttons[i].GetComponentInChildren<Text>();
                _buttonLayout[i] = _buttons[i].GetComponent<LayoutElement>();
            }
        }

        private void SetDefaultWidth()
        {
            for (int i = 0; i < _buttonLayout.Length; i++)
            {
                _buttonLayout[i].preferredWidth = DefautWidth;
            }
        }
        
        public void SetTitle(string newHeader)
        {
            _title.text = newHeader;
        }

        public void SetBody(string newBody)
        {
            _body.text = newBody;
        }

        public void SetButtons(string[] buttonsText, UnityAction[] actions)
        {
            for (int i = 0; i < buttonsText.Length; i++)
            {
                _buttons[i].onClick.RemoveAllListeners();
                _buttons[i].onClick.AddListener(actions[i]);
                _buttonsText[i].text = buttonsText[i];
                _buttons[i].gameObject.SetActive(true);
            }
            for (int i = buttonsText.Length; i < _buttons.Length; i++)
            {
                _buttons[i].gameObject.SetActive(false);
            }
            SetDefaultWidth();
        }

        public void SetButtonsPreferredWidth(float[] preferredWidthPrior)
        {
            SetDefaultWidth();
            for (int i = 0; i < preferredWidthPrior.Length; i++)
            {
                _buttonLayout[i].preferredWidth *= preferredWidthPrior[i];
            }
        }

        public void Show()
        {
            gameObject.SetActive(true);
        }
        
        public void Close()
        {
            gameObject.SetActive(false);
        }
        
    }
}