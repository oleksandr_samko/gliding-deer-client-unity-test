using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace Task3PopupSystem
{
    public class PopupManager : MonoBehaviour
    {
        private Popup _popup;
        private string _nameResPopup = "Popup";

        public int MaxButtons { get; } = 5;

        private void Load()
        {
            GameObject go = Resources.Load<GameObject>(_nameResPopup);
            go = Instantiate(go);
            _popup = go.GetComponent<Popup>();
        }

        public void Close()
        {
            if (_popup == null)
            {
                return;
            }
            
            _popup.Close();
            Destroy(_popup);
        }

        public void Show(string title, string body, string[] buttonsText, UnityAction[] actions, float[] prefWidthPrior = null)
        {
            if (buttonsText.Length > MaxButtons || actions.Length > MaxButtons || (prefWidthPrior !=null && prefWidthPrior.Length > MaxButtons))
            {
                throw new ArgumentException("Buttons amount more than " + MaxButtons);
            }

            if (buttonsText.Length != actions.Length || (prefWidthPrior !=null && prefWidthPrior.Length != buttonsText.Length))
            {
                throw new ArgumentException("Not every button set " + buttonsText.Length);
            }

            Load();
            _popup.SetTitle(title);
            _popup.SetBody(body);
            _popup.SetButtons(buttonsText, actions);
            if (prefWidthPrior != null)
            {
                _popup.SetButtonsPreferredWidth(prefWidthPrior);
            }
            _popup.Show();
        }
    }
}