using System.Collections;
using System.Collections.Generic;
using Task3PopupSystem;
using UnityEngine;
using UnityEngine.Events;

public class ScenarioPopup : MonoBehaviour
{
    void Start()
    {
        PopupManager popupMng = FindObjectOfType<PopupManager>();
        
        string[] buttonTexts = {"See more info", "Cancel", "Ok"};
        UnityAction[] actions =
        {
            delegate { Debug.Log("See more action"); },
            delegate
            {
                Debug.Log("Cancel action");
                popupMng.Close();
            },
            delegate { Debug.Log("Ok action"); }
        };
        float[] prioritiesButtons = {3, 2, 1};
        
        popupMng.Show("Pop title", "Add any body",buttonTexts,actions,prioritiesButtons);
    }
}